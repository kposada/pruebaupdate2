// Inital app
const {electron, app, BrowserWindow, Menu, protocol, ipcMain} = require("electron");
const updater = require("electron-updater");
const autoUpdater = updater.autoUpdater;




let template = []
if (process.platform === 'darwin') {
  // OS X
  const name = app.getName();
  template.unshift({
    label: name,
    submenu: [
      {
        label: 'About ' + name,
        role: 'about'
      },
      {
        label: 'Quit',
        accelerator: 'Command+Q',
        click() { app.quit(); }
      },
    ]
  })
}




let win;

function sendStatusToWindow(text) {
  log.info(text);
  win.webContents.send('message', text);
}
function createDefaultWindow() {
  win = new BrowserWindow();
  win.webContents.openDevTools();
  win.on('closed', () => {
    win = null;
  });
  win.loadURL(`file://${__dirname}/index.html#v${app.getVersion()}`);
  return win;
}

app.on('ready', function() {
    // Create the Menu
    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);
  
    createDefaultWindow();
  });
  app.on('window-all-closed', () => {
    app.quit();
  });

///////////////////
// Auto upadater //
///////////////////
//autoUpdater.requestHeaders = { "PRIVATE-TOKEN": "i4jvfzT1A7FN_5Ssz1Cv" };
autoUpdater.autoDownload = true;

autoUpdater.setFeedURL({
    provider: "generic",
    channel: "latest",
    //url: "https://gitlab.com/kposada/api/v4/projects/14719516?ref=master&private_token=i4jvfzT1A7FN_5Ssz1Cv"
    url: "https://gitlab.com/kposada/api/v4/projects/14741818/jobs/artifacts/master/raw/dist?job=build"
    //url: "https://gitlab.com/kposada/testAutoUpdate/-/jobs/artifacts/master/download?job=build"
    //url: "https://gitlab.com/kposada/14719516/-/jobs/artifacts/master/raw/dist?job=build"
});

autoUpdater.on('checking-for-update', function () {
    sendStatusToWindow('Checking for update...');
});

autoUpdater.on('update-available', function (info) {
    sendStatusToWindow('Update available.');
});

autoUpdater.on('update-not-available', function (info) {
    sendStatusToWindow('Update not available.');
});

autoUpdater.on('error', function (err) {
    sendStatusToWindow('Error in auto-updater.' + err);
});

autoUpdater.on('download-progress', function (progressObj) {
    let log_message = "Download speed: " + progressObj.bytesPerSecond;
    log_message = log_message + ' - Downloaded ' + parseInt(progressObj.percent) + '%';
    log_message = log_message + ' (' + progressObj.transferred + "/" + progressObj.total + ')';
    sendStatusToWindow(log_message);
});

autoUpdater.on('update-downloaded', function (info) {
    sendStatusToWindow('Update downloaded; will install in 1 seconds');
});

autoUpdater.on('update-downloaded', function (info) {
    setTimeout(function () {
        autoUpdater.quitAndInstall();
    }, 1000);
});

autoUpdater.checkForUpdates();

function sendStatusToWindow(message) {
    console.log(message);
}